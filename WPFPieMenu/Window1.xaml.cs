using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using WindowsInput;

namespace WPFPieMenu
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>

    public partial class Window1 : System.Windows.Window
    {
        public Window1()
        {
            InitializeComponent();
            Topmost = true;

            for (int i = 1; i <= 5; i++)
            {
                AddNewItem(i);
            }
        }

        private void AddNewItem(int n)
        {
            Image img = new Image();
            img.Height = 50;
            img.Width = 50;
            switch (n)
            {
                case 1:
                    img.Source = new BitmapImage(new Uri(@"C:\Users\jrpur\Source\Repos\pie\WPFPieMenu\Items\1.png"));
                    img.MouseLeftButtonUp += Image1_MouseLeftButtonUp;
                    pieGrid.Children.Add(img);
                    break;
                case 2:
                    img.Source = new BitmapImage(new Uri(@"C:\Users\jrpur\Source\Repos\pie\WPFPieMenu\Items\2.png"));
                    img.MouseLeftButtonUp += Image2_MouseLeftButtonUp;
                    pieGrid.Children.Add(img);
                    break;
                case 3:
                    Label time = new Label();
                    DateTime timeStamp = DateTime.Now;
                    time.Content = string.Format("{0}\r\n{1}", timeStamp.ToShortDateString(), timeStamp.ToLongTimeString());
                    pieGrid.Children.Add(time);
                    break;
                case 4:
                    img.Source = new BitmapImage(new Uri(@"C:\Users\jrpur\Source\Repos\pie\WPFPieMenu\Items\4.png"));
                    img.MouseLeftButtonUp += Image4_MouseLeftButtonUp;
                    pieGrid.Children.Add(img);
                    break;
                case 5:
                    Slider hslider = new Slider();
                    hslider.Orientation = Orientation.Horizontal;
                    hslider.AutoToolTipPlacement =
                      AutoToolTipPlacement.TopLeft;
                    hslider.AutoToolTipPrecision = 0;
                    hslider.Width = 60;
                    hslider.IsMoveToPointEnabled = false;
                    hslider.SelectionStart = 1.1;
                    hslider.SelectionEnd = 3;
                    hslider.IsSelectionRangeEnabled = false;
                    hslider.IsSnapToTickEnabled = false;
                    hslider.Background = Brushes.LightSlateGray;
                    pieGrid.Children.Add(hslider);
                    break;
            }
        }

        private void Label_Loaded(object sender, RoutedEventArgs e)
        {
            // ... Get label.
            var label = sender as Label;
            // ... Set date in content.
            label.Content = DateTime.Now.ToShortTimeString();
            //label.Content = DateTime.Now.ToShortDateString();
        }

        private void Image1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("You clicked option 1");
            Process.Start("Notepad.exe");
        }

        private void Image2_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("You clicked option 2");
            Process.Start("mspaint.exe");
        }

        private void Image4_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            /* NOTE: MENU, LMENU, and RMENU are ALT, Left ALT, and Right ALT, respectively */
            InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.LWIN, VirtualKeyCode.TAB);
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            this.Topmost = true;
            this.Activate();
        }
    }
}