using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Media.Effects;

namespace WPFPieMenu
{
  public static class WindowExtensions
    {
        #region Public Methods

        public static bool ActivateCenteredToMouse(this Window window)
        {
            ComputeTopLeft(ref window);
            return window.Activate();
        }

        public static void ShowCenteredToMouse(this Window window)
        {
            // in case the default start-up location isn't set to Manual
            WindowStartupLocation oldLocation = window.WindowStartupLocation;
            // set location to manual -> window will be placed by Top and Left property
            window.WindowStartupLocation = WindowStartupLocation.Manual;
            ComputeTopLeft(ref window);
            window.Show();
            window.WindowStartupLocation = oldLocation;
        }

        #endregion

        #region Methods

        private static void ComputeTopLeft(ref Window window)
        {
            W32Point pt = new W32Point();
            if (!GetCursorPos(ref pt))
            {
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            }

            // 0x00000002: return nearest monitor if pt is not contained in any monitor.
            IntPtr monHandle = MonitorFromPoint(pt, 0x00000002);
            W32MonitorInfo monInfo = new W32MonitorInfo();
            monInfo.Size = Marshal.SizeOf(typeof(W32MonitorInfo));

            if (!GetMonitorInfo(monHandle, ref monInfo))
            {
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            }

            // use WorkArea struct to include the taskbar position.
            W32Rect monitor = monInfo.WorkArea;
            double offsetX = Math.Round(window.Width / 2);
            double offsetY = Math.Round(window.Height / 2);

            double top = pt.Y - offsetY;
            double left = pt.X - offsetX;

            Rect screen = new Rect(
                new Point(monitor.Left, monitor.Top),
                new Point(monitor.Right, monitor.Bottom));
            Rect wnd = new Rect(
                new Point(left-29, top),
                new Point(left + window.Width, top + window.Height-120));
       
            window.Top = wnd.Top;
            window.Left = wnd.Left;

            if (!screen.Contains(wnd))
            {
                if (wnd.Top < screen.Top)
                {
                    double diff = Math.Abs(screen.Top - wnd.Top);
                    window.Top = wnd.Top + diff;
                }

                if (wnd.Bottom > screen.Bottom)
                {
                    double diff = wnd.Bottom - screen.Bottom;
                    window.Top = wnd.Top - diff;
                }

                if (wnd.Left < screen.Left)
                {
                    double diff = Math.Abs(screen.Left - wnd.Left);
                    window.Left = wnd.Left + diff;
                }

                if (wnd.Right > screen.Right)
                {
                    double diff = wnd.Right - screen.Right;
                    window.Left = wnd.Left - diff;
                }
            }
        }

        #endregion

        #region W32 API

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(ref W32Point pt);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetMonitorInfo(IntPtr hMonitor, ref W32MonitorInfo lpmi);

        [DllImport("user32.dll")]
        private static extern IntPtr MonitorFromPoint(W32Point pt, uint dwFlags);

        [StructLayout(LayoutKind.Sequential)]
        public struct W32Point
        {
            public int X;
            public int Y;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct W32MonitorInfo
        {
            public int Size;
            public W32Rect Monitor;
            public W32Rect WorkArea;
            public uint Flags;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct W32Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        #endregion
    }
    /// <summary>
    /// 
// MAIN CODE FOR THE WINDOW HOTKEYS
    /// </summary>
    public partial class Window1 : Window
    {
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        private const int HOTKEY_ID = 9000;

        //Modifiers:
        private const uint MOD_NONE = 0x0000; //(none)
        private const uint MOD_ALT = 0x0001; //ALT
        private const uint MOD_CONTROL = 0x0002; //CTRL
        private const uint MOD_SHIFT = 0x0004; //SHIFT
        private const uint MOD_WIN = 0x0008; //WINDOWS
        //CAPS LOCK:
        private const uint VK_CAPITAL = 0x14;
        //MIDDLE MOUSE
        private const uint MK_MBUTTON = 0x0010;

        private IntPtr _windowHandle;
        private HwndSource _source;
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            _windowHandle = new WindowInteropHelper(this).Handle;
            _source = HwndSource.FromHwnd(_windowHandle);
            _source.AddHook(HwndHook);

            RegisterHotKey(_windowHandle, HOTKEY_ID, MOD_CONTROL, VK_CAPITAL); //CTRL + CAPS_LOCK
        }

        private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_HOTKEY = 0x0312;
            switch (msg)
            {
                case WM_HOTKEY:
                    switch (wParam.ToInt32())
                    {
                        case HOTKEY_ID:
                            int vkey = (((int)lParam >> 16) & 0xFFFF);
                            if (vkey == VK_CAPITAL)
                            {
                                if (WindowState == WindowState.Minimized)
                                {
                                    this.ShowCenteredToMouse();
                                    WindowState = WindowState.Normal;
                                }
                                else
                                WindowState = WindowState.Minimized;
                            }
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        protected override void OnClosed(EventArgs e)
        {
            _source.RemoveHook(HwndHook);
            UnregisterHotKey(_windowHandle, HOTKEY_ID);
            base.OnClosed(e);
        }
    }


    //PIE MENU CLASSES
    public class PieMenu : Panel
    {
        public static readonly DependencyProperty ClippingRadiusProperty;

        private bool ismouseleftdown = false;

        static PieMenu()
        {
            PieMenu.ClippingRadiusProperty = DependencyProperty.Register("ClippingRadius", typeof(double), typeof(PieMenu), new FrameworkPropertyMetadata(20.0));
        }

        protected override Size MeasureOverride(Size availablesize)
        {
            foreach (UIElement uie in Children)
                uie.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));

            return availablesize;
        }

        protected override Size ArrangeOverride(Size finalsize)
        {
            double radx = this.DesiredSize.Width / 2.0;
            double rady = this.DesiredSize.Height / 2.0;
            Point center = new Point(radx, rady);

            double angle = 0.0, anglestep = 0.0;

            if (this.Children.Count != 0.0)
                anglestep = 360.0 / (double)this.Children.Count;

            double deg2rad = Math.PI / 180.0;

            foreach (UIElement uie in Children)
            {
                double a = (angle + anglestep / 2.0) * deg2rad;

                uie.Arrange(new Rect(Point.Add(center, new Vector((radx + (double)base.GetValue(PieMenu.ClippingRadiusProperty)) * Math.Cos(a) / 2.0 - uie.DesiredSize.Width / 2.0, (rady + (double)base.GetValue(PieMenu.ClippingRadiusProperty)) * Math.Sin(a) / 2.0 - uie.DesiredSize.Height / 2.0)), uie.DesiredSize));

                angle += anglestep;
            }

            return finalsize;
        }

        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            base.OnMouseEnter(e);

            this.InvalidateVisual();
        }

        protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {
            base.OnMouseLeave(e);

            this.InvalidateVisual();
        }

        protected override void OnMouseLeftButtonDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            this.ismouseleftdown = true;

            this.InvalidateVisual();
        }

        protected override void OnMouseLeftButtonUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            this.ismouseleftdown = false;

            this.InvalidateVisual();
        }

        protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
        {
            base.OnMouseMove(e);

            this.InvalidateVisual();
        }


        protected override void OnRender(DrawingContext dc)
        {
            double radx = this.DesiredSize.Width / 2.0;
            double rady = this.DesiredSize.Height / 2.0;
            Point center = new Point(radx, rady);

            double angle = 0.0, anglestep = 0.0;

            if (this.Children.Count != 0.0)
                anglestep = 360.0 / (double)this.Children.Count;

            double deg2rad = Math.PI / 180.0;

            dc.PushClip(new CombinedGeometry(GeometryCombineMode.Exclude, new EllipseGeometry(center, radx + 1, rady + 1), new EllipseGeometry(center, (double)base.GetValue(PieMenu.ClippingRadiusProperty), (double)base.GetValue(PieMenu.ClippingRadiusProperty))));

            dc.DrawEllipse(new SolidColorBrush(Color.FromRgb(250, 250, 250)), new Pen(new SolidColorBrush(Color.FromRgb(197, 197, 197)), 1.0), center, radx, rady);

            if ((double)base.GetValue(PieMenu.ClippingRadiusProperty) > 0.0)
                dc.DrawEllipse(null, new Pen(new SolidColorBrush(Color.FromRgb(197, 197, 197)), 1.0), center, (double)base.GetValue(PieMenu.ClippingRadiusProperty) + 1, (double)base.GetValue(PieMenu.ClippingRadiusProperty) + 1);

            double sa = 0.0, ea = 0.0;

            foreach (UIElement uie in this.Children)
            {
                double a = angle * deg2rad;
                dc.DrawLine(new Pen(new SolidColorBrush(Color.FromRgb(197, 197, 197)), 1.0), center, Point.Add(center, new Vector(radx * Math.Cos(a), rady * Math.Sin(a))));

                if (this.IsMouseOver && uie.IsMouseOver)
                {
                    sa = a;
                    ea = sa + anglestep * deg2rad;
                }

                angle += anglestep;
            }

            if (this.IsMouseOver)
            {
                PathGeometry path = new PathGeometry();
                PathFigure pathfig = new PathFigure();
                pathfig.StartPoint = center;
                pathfig.Segments.Add(new LineSegment(Point.Add(center, new Vector(radx * Math.Cos(sa), rady * Math.Sin(sa))), true));
                pathfig.Segments.Add(new ArcSegment(Point.Add(center, new Vector(radx * Math.Cos(ea), rady * Math.Sin(ea))), new Size(1.0, 1.0), 0.0, false, SweepDirection.Clockwise, true));
                pathfig.Segments.Add(new LineSegment(center, true));
                path.Figures.Add(pathfig);
                dc.PushClip(path);

                LinearGradientBrush brush = new LinearGradientBrush();
                brush.StartPoint = new Point(0, 0);
                brush.EndPoint = new Point(0, 1);

                angle = 0;
                if (this.ismouseleftdown)
                {
                    brush.GradientStops.Add(new GradientStop(Color.FromRgb(254, 216, 170), 0.0));
                    brush.GradientStops.Add(new GradientStop(Color.FromRgb(251, 181, 101), 0.4));
                    brush.GradientStops.Add(new GradientStop(Color.FromRgb(250, 157, 52), 0.4));
                    brush.GradientStops.Add(new GradientStop(Color.FromRgb(253, 238, 170), 1.0));

                    dc.DrawEllipse(brush, new Pen(new SolidColorBrush(Color.FromRgb(171, 161, 140)), 1.0), center, radx, rady);
                    dc.DrawEllipse(null, new Pen(new LinearGradientBrush(Color.FromRgb(223, 183, 136), Colors.Transparent, 45.0), 1.0), center, radx - 1, rady - 1);

                    if ((double)base.GetValue(PieMenu.ClippingRadiusProperty) > 0.0)
                        dc.DrawEllipse(null, new Pen(new SolidColorBrush(Color.FromRgb(171, 161, 140)), 1.0), center, (double)base.GetValue(PieMenu.ClippingRadiusProperty) + 1, (double)base.GetValue(PieMenu.ClippingRadiusProperty) + 1);

                    foreach (UIElement uie in this.Children)
                    {
                        double a = angle * deg2rad;
                        dc.DrawLine(new Pen(new SolidColorBrush(Color.FromRgb(171, 161, 140)), 1.0), center, Point.Add(center, new Vector(radx * Math.Cos(a), rady * Math.Sin(a))));

                        angle += anglestep;
                    }
                }
                else
                {
                    brush.GradientStops.Add(new GradientStop(Color.FromRgb(255, 254, 227), 0.0));
                    brush.GradientStops.Add(new GradientStop(Color.FromRgb(255, 231, 151), 0.4));
                    brush.GradientStops.Add(new GradientStop(Color.FromRgb(255, 215, 80), 0.4));
                    brush.GradientStops.Add(new GradientStop(Color.FromRgb(255, 231, 150), 1.0));

                    dc.DrawEllipse(brush, new Pen(new SolidColorBrush(Color.FromRgb(210, 192, 141)), 1.0), center, radx, rady);
                    dc.DrawEllipse(null, new Pen(new LinearGradientBrush(Color.FromRgb(255, 255, 247), Colors.Transparent, 45.0), 1.0), center, radx - 1, rady - 1);

                    if ((double)base.GetValue(PieMenu.ClippingRadiusProperty) > 0.0)
                        dc.DrawEllipse(null, new Pen(new SolidColorBrush(Color.FromRgb(210, 192, 141)), 1.0), center, (double)base.GetValue(PieMenu.ClippingRadiusProperty) + 1, (double)base.GetValue(PieMenu.ClippingRadiusProperty) + 1);

                    foreach (UIElement uie in this.Children)
                    {
                        double a = angle * deg2rad;
                        dc.DrawLine(new Pen(new SolidColorBrush(Color.FromRgb(210, 192, 141)), 1.0), center, Point.Add(center, new Vector(radx * Math.Cos(a), rady * Math.Sin(a))));

                        angle += anglestep;
                    }
                }

                dc.Pop();
            }

            dc.Pop();
        }

        [Bindable(true)]
        public double ClippingRadius
        {
            get
            {
                return (double)base.GetValue(PieMenu.ClippingRadiusProperty);
            }
            set
            {
                base.SetValue(PieMenu.ClippingRadiusProperty, value);
            }
        }
    }
}
